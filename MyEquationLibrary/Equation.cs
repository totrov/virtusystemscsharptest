﻿using System;
using System.Linq;
using MyEquationLibrary.Infrastructure.Exceptions;

namespace MyEquationLibrary
{
    public class Equation
    {
        public Polynom RightPolynom { get; private set; }
        public Polynom LeftPolynom { get; private set; }
        public Equation(string equationString)
        {
            ValidateEquationString(equationString);
            var leftAndRightPolynomStrings = equationString.Split('=');
            LeftPolynom = new Polynom(leftAndRightPolynomStrings[0]);
            RightPolynom = new Polynom(leftAndRightPolynomStrings[1]);
        }

        private Equation(Polynom leftExpression, Polynom rightExpression)
        {
            LeftPolynom = leftExpression;
            RightPolynom = rightExpression;
        }

        private void ValidateEquationString(string equationString)
        {
            if (equationString == null)
                throw new ArgumentNullException("Equation string is null");
            if (equationString.Count(x => x == '=') != 1)
                throw new EquationInputException("Equality sign missed or comes up more than once");
        }

        public override string ToString()
        {
            return LeftPolynom + " = " + RightPolynom;
        }

        public void ConvertToCanonical()
        {
            RightPolynom.ConvertToOpposite();
            LeftPolynom.Append(RightPolynom);
            LeftPolynom.ConvertToCanonical();
            RightPolynom = new Polynom();
        }
    }
}
