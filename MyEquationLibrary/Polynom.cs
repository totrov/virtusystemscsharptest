using System;
using System.Collections.Generic;
using System.Linq;
using MyEquationLibrary.MonomialCode;

namespace MyEquationLibrary
{
    public class Polynom
    {
        private readonly List<Monomial> _monomials;

        public Polynom(string expressionString)
        {
            ValidateInput(expressionString);
            _monomials = new List<Monomial>();
            var currentMonomial = new Monomial();
            foreach (var ch in expressionString)
                ProcessChar(ch, ref currentMonomial, _monomials);
            currentMonomial.Finish();
            _monomials.Add(currentMonomial);
        }

        private void ProcessChar(char ch, ref Monomial currentMonomial, List<Monomial> monomials)
        {
            currentMonomial.ProcessChar(ch);
            if (!currentMonomial.IsFinished) return;

            _monomials.Add(currentMonomial);
            currentMonomial = new Monomial();
            if (ch == '-')
                currentMonomial.ProcessChar(ch);
        }

        private void ValidateInput(string expressionString)
        {
            if (expressionString == null)
                throw new ArgumentNullException();
            //todo add validations
        }

        internal Polynom()
            : this("0")
        { }

        public void ConvertToOpposite()
        {
            _monomials.ForEach(x => x.ConvertToOpposite());
        }

        public void Append(Polynom polynom)
        {
            _monomials.AddRange(polynom._monomials);
        }

        public void ConvertToCanonical()
        {
            Monomial.ReductSimilarTerms(_monomials);
        }

        public override string ToString()
        {
            return _monomials.Select(x => x.ToString()).Aggregate((x, y) => x + (y.StartsWith("-") ? " " : " +") + y);
        }
    }
}
