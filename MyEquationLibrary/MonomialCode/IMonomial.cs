namespace MyEquationLibrary.MonomialCode
{
    internal interface IMonomial : IMonomialStateKeeper
    {
        IMonomialData GetData();
        bool IsFinished { get; }
    }
}