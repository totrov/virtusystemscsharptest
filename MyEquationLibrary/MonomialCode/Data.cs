using System;
using System.Collections.Generic;
using System.Globalization;
using MyEquationLibrary.Infrastructure.Exceptions;

namespace MyEquationLibrary.MonomialCode
{
    internal class Data : IMonomialData
    {
        public double Coefficient { get; set; }
        public Queue<char> WorkingQueue { get; private set; }
        public void AddVariable(string name, double power)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, double> VariablePowerDictionary { get; private set; }
        public Data()
        {
            WorkingQueue = new Queue<char>();
            Coefficient = 0;
        }

        public void DequeueCoefficient()
        {
            Coefficient = Double.Parse(new string(WorkingQueue.ToArray()), CultureInfo.InvariantCulture);
            WorkingQueue.Clear();
        }

        public void DequeueVariable()
        {
            var variable = new string(WorkingQueue.ToArray());
            if (VariablePowerDictionary == null)
                VariablePowerDictionary = new Dictionary<string, double>();
            if (VariablePowerDictionary.ContainsKey(variable))
                throw new ParseException("Variable " + variable + " appears twice");
            VariablePowerDictionary.Add(variable, 1);
            WorkingQueue.Clear();
        }

    }
}