using MyEquationLibrary.CharProcessors;

namespace MyEquationLibrary.MonomialCode.State
{
    internal class VariableState : AMonomialState, IMonomialState
    {
        public VariableState(IMonomial monomial) : base(monomial) { }

        public void ProcessChar(char ch)
        {
            CharProcessorBase.ResolveByChar(ch, _monomial).ProcessVariableState(ch);
        }

        public void FinishProcessing()
        {
            _monomial.GetData().DequeueVariable();
            _monomial.SetProcessingState(_monomial.GetFinishedState());
        }
    }
}