namespace MyEquationLibrary.MonomialCode.State
{
    internal interface IMonomialState
    {
        void ProcessChar(char ch);
        void FinishProcessing();
    }

    internal abstract class AMonomialState
    {
        protected readonly IMonomial _monomial;
        internal AMonomialState(IMonomial monomial)
        {
            _monomial = monomial;
        }
    }
}