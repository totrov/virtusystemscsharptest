using MyEquationLibrary.CharProcessors;

namespace MyEquationLibrary.MonomialCode.State
{
    internal class CoefficientFullDeterminedState : AMonomialState, IMonomialState
    {
        public CoefficientFullDeterminedState(IMonomial monomial) : base(monomial) { }

        public void ProcessChar(char ch)
        {
            CharProcessorBase.ResolveByChar(ch, _monomial).ProcessCoefficientDotFullDeterminedState(ch);
        }

        public void FinishProcessing()
        {
            _monomial.GetData().DequeueCoefficient();
            _monomial.SetProcessingState(_monomial.GetFinishedState());
        }
    }
}