using System;

namespace MyEquationLibrary.MonomialCode.State
{
    internal class FinishedState : AMonomialState,IMonomialState
    {
        public FinishedState(Monomial monomial) : base(monomial){}

        public void ProcessChar(char ch)
        {
            throw new NotImplementedException();
        }

        public void FinishProcessing()
        {
            throw new NotImplementedException();
        }
    }
}