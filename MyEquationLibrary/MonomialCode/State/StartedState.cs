using MyEquationLibrary.CharProcessors;
using MyEquationLibrary.Infrastructure.Exceptions;

namespace MyEquationLibrary.MonomialCode.State
{
    internal class StartedState : AMonomialState, IMonomialState
    {
        public StartedState(IMonomial monomial) : base(monomial) { }

        public void ProcessChar(char ch)
        {
            CharProcessorBase.ResolveByChar(ch, _monomial).ProcessStartedState(ch);
        }

        public void FinishProcessing()
        {
            throw new ParseException("Cannot finish parsing monomial without passed symbols");
        }
    }
}