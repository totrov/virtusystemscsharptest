using MyEquationLibrary.CharProcessors;

namespace MyEquationLibrary.MonomialCode.State
{
    internal class CoefficientWithoutDotState : AMonomialState, IMonomialState
    {
        public CoefficientWithoutDotState(IMonomial monomial) : base(monomial) { }

        public void ProcessChar(char ch)
        {
            CharProcessorBase.ResolveByChar(ch, _monomial).ProcessCoefficientWithoutDotState(ch);
        }
        public void FinishProcessing()
        {
            _monomial.GetData().DequeueCoefficient();
            _monomial.SetProcessingState(_monomial.GetFinishedState());
        }
    }
}