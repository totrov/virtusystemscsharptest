using MyEquationLibrary.CharProcessors;
using MyEquationLibrary.Infrastructure.Exceptions;

namespace MyEquationLibrary.MonomialCode.State
{
    internal class DotProcessedState : AMonomialState, IMonomialState
    {
        public DotProcessedState(IMonomial monomial) : base(monomial) { }

        public void ProcessChar(char ch)
        {
            CharProcessorBase.ResolveByChar(ch, _monomial).ProcessDotProcessedState(ch);
        }
        public void FinishProcessing()
        {
            throw new ParseException("Cannot finish parsing without digits after dot");
        }
    }
}