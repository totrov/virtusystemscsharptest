using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using MyEquationLibrary.MonomialCode.State;

namespace MyEquationLibrary.MonomialCode
{
    internal class Monomial : IMonomial
    {
        #region Fields & Props

        private readonly Data _data;
        private IMonomialState _state;
        public bool IsFinished { get; private set; }

        #region ConcreteStateFields

        private readonly IMonomialState _startedState;
        private readonly IMonomialState _finishedState;
        private readonly IMonomialState _coefficientWithoutDotState;
        private readonly IMonomialState _dotProcessedState;
        private readonly IMonomialState _coefficientFullDeterminedState;
        private readonly IMonomialState _variableState;

        #endregion

        #endregion

        #region Constructors

        internal Monomial()
        {
            IsFinished = false;
            _startedState = new StartedState(this);
            _finishedState = new FinishedState(this);
            _coefficientWithoutDotState = new CoefficientWithoutDotState(this);
            _dotProcessedState = new DotProcessedState(this);
            _coefficientFullDeterminedState = new CoefficientFullDeterminedState(this);
            _variableState = new VariableState(this);
            _state = _startedState;
            _data = new Data();
        }

        #endregion

        #region Instance Methods

        internal void ProcessChar(char ch)
        {
            _state.ProcessChar(ch);
        }

        internal void Finish()
        {
            _state.FinishProcessing();
            IsFinished = true;
        }
        public IMonomialData GetData()
        {
            return _data;
        }

        public void SetProcessingState(IMonomialState state)
        {
            _state = state;
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            if (!(_data.Coefficient.Equals(1.0) && _data.VariablePowerDictionary != null && _data.VariablePowerDictionary.Any()))
                result.Append(_data.Coefficient.ToString(CultureInfo.InvariantCulture));
            if (_data.VariablePowerDictionary == null) return result.ToString();
            foreach (var varPowPair in _data.VariablePowerDictionary)
            {
                result.Append(varPowPair.Key.ToString(CultureInfo.InvariantCulture));
                if (!varPowPair.Value.Equals(1.0))
                {
                    result.Append("^");
                    result.Append(varPowPair.Value.ToString(CultureInfo.InvariantCulture));
                }
            }
            return result.ToString();
        }

        #region StateGetters

        public IMonomialState GetStartedState()
        {
            return _startedState;
        }

        public IMonomialState GetFinishedState()
        {
            return _finishedState;
        }

        public IMonomialState GetCoefficientWithoutDotState()
        {
            return _coefficientWithoutDotState;
        }

        public IMonomialState GetDotProcessedState()
        {
            return _dotProcessedState;
        }

        public IMonomialState GetCoefficientFullDeterminedState()
        {
            return _coefficientFullDeterminedState;
        }

        public IMonomialState GetVariableState()
        {
            return _variableState;
        }

        #endregion

        #endregion

        #region StaticMethods

        public Monomial GetOpposite()
        {
            throw new NotImplementedException();
        }

        public static List<Monomial> ReductSimilarTerms(List<Monomial> monomials)
        {
            throw new NotImplementedException();
        }

        public void ConvertToOpposite()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}