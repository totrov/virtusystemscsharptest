using MyEquationLibrary.MonomialCode.State;

namespace MyEquationLibrary.MonomialCode
{
    internal interface IMonomialStateKeeper
    {
        void SetProcessingState(IMonomialState state);
        IMonomialState GetStartedState();
        IMonomialState GetFinishedState();
        IMonomialState GetCoefficientWithoutDotState();
        /// <summary>
        /// dot have been just processed and there are NO processed digits after it
        /// </summary>
        IMonomialState GetDotProcessedState();
        /// <summary>
        /// dot have processed and there ARE processed digits after it
        /// </summary>
        IMonomialState GetCoefficientFullDeterminedState();
        IMonomialState GetVariableState();

    }
}