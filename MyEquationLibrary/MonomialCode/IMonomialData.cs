using System.Collections.Generic;

namespace MyEquationLibrary.MonomialCode
{
    internal interface IMonomialData
    {
        double Coefficient { get; set; }
        Queue<char> WorkingQueue { get; }
        void AddVariable(string name, double power);
        void DequeueCoefficient();
        void DequeueVariable();
    }
}