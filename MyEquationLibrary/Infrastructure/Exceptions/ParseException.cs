using System;

namespace MyEquationLibrary.Infrastructure.Exceptions
{
    public class ParseException : Exception
    {
        internal ParseException() { }
        internal ParseException(string message)
            : base(message) { }
        internal ParseException(string message, Exception exception)
            : base(message, exception) { }
    }
}