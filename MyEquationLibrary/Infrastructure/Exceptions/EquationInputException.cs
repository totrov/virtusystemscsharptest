using System;

namespace MyEquationLibrary.Infrastructure.Exceptions
{
    public class EquationInputException : Exception
    {
        internal EquationInputException() { }
        internal EquationInputException(string message)
            : base(message) { }
        internal EquationInputException(string message, Exception exception)
            : base(message, exception) { }
    }
}