using MyEquationLibrary.MonomialCode;

namespace MyEquationLibrary.CharProcessors
{
    internal class DivisionCharProcessor : CharProcessorBase
    {
        protected DivisionCharProcessor(IMonomial monomial) : base(monomial) { }
    }
}