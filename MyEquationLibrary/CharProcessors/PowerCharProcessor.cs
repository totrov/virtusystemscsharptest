using MyEquationLibrary.MonomialCode;

namespace MyEquationLibrary.CharProcessors
{
    internal class PowerCharProcessor : CharProcessorBase
    {
        protected PowerCharProcessor(IMonomial monomial) : base(monomial) { }

        public override void ProcessVariableState(char ch)
        {
            Data.DequeueVariable();
            Data.WorkingQueue.Enqueue(ch);
        }
    }
}