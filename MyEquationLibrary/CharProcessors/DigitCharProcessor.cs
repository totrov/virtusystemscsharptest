using System.Collections.Generic;
using MyEquationLibrary.Infrastructure.Exceptions;
using MyEquationLibrary.MonomialCode;

namespace MyEquationLibrary.CharProcessors
{
    internal class DigitCharProcessor : CharProcessorBase
    {
        protected DigitCharProcessor(IMonomial monomial) : base(monomial) { }

        public override void ProcessStartedState(char ch)
        {
            if (ch == '.')
                throw new EquationInputException("expression started with a dot cannot be parsed");
            Data.WorkingQueue.Enqueue(ch);
            Monomial.SetProcessingState(Monomial.GetCoefficientWithoutDotState());
        }

        public override void ProcessCoefficientWithoutDotState(char ch)
        {
            if (ch == '.')
            {
                Data.WorkingQueue.Enqueue(ch);
                Monomial.SetProcessingState(Monomial.GetDotProcessedState());
            }
            else
                Data.WorkingQueue.Enqueue(ch);
        }

        public override void ProcessDotProcessedState(char ch)
        {
            if (ch == '.')
                throw new ParseException("cannot process dot sign: same is already in coefficient");
            Data.WorkingQueue.Enqueue(ch);
            Monomial.SetProcessingState(Monomial.GetCoefficientFullDeterminedState());
        }

        public override void ProcessCoefficientDotFullDeterminedState(char ch)
        {
            if (ch == '.')
                throw new ParseException("cannot process dot sign: same is already in coefficient");
            Data.WorkingQueue.Enqueue(ch);
            Monomial.SetProcessingState(Monomial.GetCoefficientFullDeterminedState());
        }

        public override void ProcessVariableState(char ch)
        {
            Data.WorkingQueue.Enqueue(ch);
        }
    }
}