using MyEquationLibrary.MonomialCode;

namespace MyEquationLibrary.CharProcessors
{
    internal class LetterProcessor : CharProcessorBase
    {
        protected LetterProcessor(IMonomial monomial) : base(monomial) { }

        public override void ProcessStartedState(char ch)
        {
            Data.Coefficient = 1;
            Data.WorkingQueue.Enqueue(ch);
            Monomial.SetProcessingState(Monomial.GetVariableState());
        }

        public override void ProcessVariableState(char ch)
        {
            Data.DequeueVariable();
            Data.WorkingQueue.Enqueue(ch);
        }
    }
}