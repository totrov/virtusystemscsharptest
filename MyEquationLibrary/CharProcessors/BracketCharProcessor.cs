using System.Collections.Generic;
using MyEquationLibrary.MonomialCode;

namespace MyEquationLibrary.CharProcessors
{
    internal class BracketCharProcessor : CharProcessorBase
    {
        protected BracketCharProcessor(IMonomial monomial) : base(monomial) { }
    }
}