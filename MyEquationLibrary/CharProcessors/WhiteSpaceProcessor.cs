using MyEquationLibrary.MonomialCode;

namespace MyEquationLibrary.CharProcessors
{
    internal class WhiteSpaceProcessor : CharProcessorBase
    {
        protected WhiteSpaceProcessor(IMonomial monomial) : base(monomial) { }
        protected override void DefaultParseAction(char ch)
        {
            //do nothing!
        }
    }
}