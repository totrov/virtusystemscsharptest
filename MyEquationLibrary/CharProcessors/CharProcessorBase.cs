using System;
using System.Reflection;
using MyEquationLibrary.Infrastructure.Exceptions;
using MyEquationLibrary.MonomialCode;

namespace MyEquationLibrary.CharProcessors
{
    internal class CharProcessorBase
    {
        protected IMonomial Monomial;
        protected IMonomialData Data;
        internal static CharProcessorBase ResolveByChar(char ch, IMonomial monomial)
        {
            switch (ch)
            {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '.':
                    return CharProcessorSingleton<DigitCharProcessor>.GetInstance(monomial);
                case '^':
                    return CharProcessorSingleton<PowerCharProcessor>.GetInstance(monomial);
                case '/':
                    return CharProcessorSingleton<DivisionCharProcessor>.GetInstance(monomial);
                case '(':
                case ')':
                    return CharProcessorSingleton<BracketCharProcessor>.GetInstance(monomial);
                case ' ':
                    return CharProcessorSingleton<WhiteSpaceProcessor>.GetInstance(monomial);
                default:
                    return Char.IsLetter(ch)
                        ? CharProcessorSingleton<LetterProcessor>.GetInstance(monomial)
                        : CharProcessorSingleton<CharProcessorBase>.GetInstance(monomial);
            }
        }

        protected CharProcessorBase(IMonomial monomial)
        {
            Monomial = monomial;
            Data = monomial.GetData();
        }

        public virtual void ProcessStartedState(char ch)
        {
            DefaultParseAction(ch);
        }
        public virtual void ProcessCoefficientDotFullDeterminedState(char ch)
        {
            DefaultParseAction(ch);
        }
        public virtual void ProcessFinishedState(char ch)
        {
            DefaultParseAction(ch);
        }
        public virtual void ProcessCoefficientWithoutDotState(char ch)
        {
            DefaultParseAction(ch);
        }
        public virtual void ProcessDotProcessedState(char ch)
        {
            DefaultParseAction(ch);
        }
        public virtual void ProcessVariableState(char ch)
        {
            DefaultParseAction(ch);
        }
        protected virtual void DefaultParseAction(char ch)
        {
            throw new ParseException(String.Format("format error near \"{0}\" character", ch));
        }

        internal class CharProcessorSingleton<T> where T : CharProcessorBase
        {
            private static T _t;
            public static T GetInstance(IMonomial monomial)
            {
                if (_t == null)
                    return _t = (T) Activator.CreateInstance(typeof (T),
                        BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public, null,
                        new object[] {monomial},
                        null);
                if (_t.Monomial == monomial) return _t;
                _t.Monomial = monomial;
                _t.Data = monomial.GetData();
                return _t;
            }
        }

        internal enum DequeueObject
        {
            Coefficient,
            Variable
        }
    }
}