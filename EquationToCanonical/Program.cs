﻿using System;
using MyEquationLibrary;

namespace EquationToCanonical
{
    class Program
    {
        static void Main(string[] args)
        {
            Equation equation = null;
            while (equation == null)
            {
                Console.WriteLine("Enter the input equation:");
                var inputEquation = Console.ReadLine();
                equation = CreateEquationByString(inputEquation);
            }
            equation.ConvertToCanonical();
            var result = equation.ToString();
            Console.WriteLine("Canonical form: {0}",result);
            Console.ReadKey(true);
        }

        private static Equation CreateEquationByString(string equationString)
        {
            try
            {
                return new Equation(equationString);
            }
            //тут будут обрабатываться специфические исключения, которых пока нет =)
            catch (Exception)
            {
                return null;
            }
        }
    }

}
