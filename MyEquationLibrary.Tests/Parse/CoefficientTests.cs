﻿using MyEquationLibrary.Infrastructure.Exceptions;
using NUnit.Framework;

namespace MyEquationLibrary.Tests.Parse
{
    [TestFixture]
    public class CoefficientTests
    {
        [Test, Sequential]
        public void CoefficientTest_SingleDigit(
            [Values("1 = 5", "4=   7", "7    =6", "  3     =   4  ")]string input,
            [Values("1 = 5", "4 = 7", "7 = 6", "3 = 4")]string output)
        {
            //Arrange
            //Act
            var result = new Equation(input).ToString();
            //Assert
            Assert.AreEqual(result, output);
        }

        [Test, Sequential]
        public void CoefficientTest_MultipleDigit(
            [Values("12 = 88", "76 =7", "3=  44 ", "   88     =   34534")] string input,
            [Values("12 = 88", "76 = 7", "3 = 44", "88 = 34534")] string output)
        {
            //Act
            var result = new Equation(input).ToString();
            //Assert
            Assert.AreEqual(result, output);
        }

        [Test, Sequential]
        public void CoefficientTest_WithDot_Valid(
            [Values("12.1 = 88", "76 =7.1", "3=  44.1345 ", "   88.144     =   34534.655")] string input,
            [Values("12.1 = 88", "76 = 7.1", "3 = 44.1345", "88.144 = 34534.655")] string output)
        {
            //Act
            var result = new Equation(input).ToString();
            //Assert
            Assert.AreEqual(result, output);
        }

        [Test]
        public void CoefficientTest_WithDot_NotValid(
            [Values("12. = 88", "76 =7.1.", "3=  44.13.45 ", "   88.14.4     =   34534.655")] string input)
        {
            Assert.Throws<ParseException>(() => { new Equation(input); });
        }
    }
}

//[Test]
//public void ()
//{
//    //Arrange
//    //Act
//    //Assert
//}