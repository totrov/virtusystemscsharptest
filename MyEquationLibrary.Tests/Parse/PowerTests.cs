﻿using NUnit.Framework;

namespace MyEquationLibrary.Tests.Parse
{
    [TestFixture]
    public class PowerTests
    {
        [Test, Sequential]
        public void PowerSingleVarTest(
            [Values("x^3 = 5", "x323 ^ 5  6=   7")]string input,
            [Values("x^3 = 5", "x323^56 = 7")] string output)
        {
            //Arrange
            //Act
            var result = new Equation(input).ToString();
            //Assert
            Assert.AreEqual(result, output);
        }
    }
}
