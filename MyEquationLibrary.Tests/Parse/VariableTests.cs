﻿using NUnit.Framework;

namespace MyEquationLibrary.Tests.Parse
{
    [TestFixture]
    public class VariableTests
    {
        [Test, Sequential]
        public void SingleOneSignVariable(
            [Values("x = 5", "x=   7", "x    =y", "  x     =   y  ")]string input,
            [Values("x = 5", "x = 7", "x = y", "x = y")] string output)
        {
            //Arrange
            //Act
            var result = new Equation(input).ToString();
            //Assert
            Assert.AreEqual(result, output);
        }

        [Test, Sequential]
        public void SingleMultipleSignVariable(
            [Values("x123 = 5", "x5345=   7", "x8756 7   =y2", "  x 2   2 =   y 33 ")]string input,
            [Values("x123 = 5", "x5345 = 7", "x87567 = y2", "x22 = y33")] string output)
        {
            //Arrange
            //Act
            var result = new Equation(input).ToString();
            //Assert
            Assert.AreEqual(result, output);
        }
        [Test, Sequential]
        public void MultipleVariables(
            [Values("xy123 = 5", "x53y45=   7", "xy8756 7   =y2", "  x u =   y o ")]string input,
            [Values("xy123 = 5", "x53y45 = 7", "xy87567 = y2", "xu = yo")] string output)
        {
            //Arrange
            //Act
            var result = new Equation(input).ToString();
            //Assert
            Assert.AreEqual(result, output);
        }
    }
}
